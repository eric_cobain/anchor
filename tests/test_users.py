import base64
from flask import json
from app.models import User

def test_create_new_user(db, session, client):
    """Attempt to create a basic user."""

    data = {'username': 'You', 'email': 'you@example.com',
            'password': 'foobar'}

    response = client.post('/users', data=data)
    assert response.status_code == 201
    assert 'Location' in response.headers

    user = User.query.filter(User.username == data['username']).one()

    assert  '/users/{}'.format(user.id) in response.headers['Location']

def test_create_invalid_user(db, session, client):
    """ Try to create a user with invalid/missing information."""

    data = {'email': 'you@example.com'}
    response = client.post('/users', data=data)

    assert response.status_code == 400
    assert 'message' in response.json
    assert 'username' in response.json['message']

def test_get_single_user_authenticated(db, session, client):
    """Attempt to fetch a user."""

    data = {'username': 'authed', 'email': 'authed@example.com',
            'password': 'foobar'}
    user = User(**data)
    session.add(user)
    session.commit()

    creds = base64.b64encode(
        b'{0}:{1}'.format(
            user.username, data['password'])).decode('utf-8')

    response = client.get('/users/{}'.format(user.id),
               headers={'Authorization': 'Basic ' + creds})

    assert response.status_code == 200
    assert json.loads(response.get_data())['id'] == user.id

def test_get_single_user_unauthenticated(db, session, client):
    """Attempt to fetch single user while unauthenticated."""

    data = {'username': 'authed', 'email': 'authed@example.com',
            'password': 'foobar'}
    user = User(**data)
    session.add(user)
    session.commit()

    response = client.get('/users/{}'.format(user.id))
    assert response.status_code == 401

def test_get_single_user_unauthorized(db, session, client):
    """Attempt to fetch a user using different users credentials."""

    alice_data = {'username': 'alice', 'email': 'alice@example.com',
        'password': 'foobar'}
    bob_data = {'username': 'bob', 'email': 'bob@example.com',
        'password': 'foobar'}
    alice = User(**alice_data)
    bob = User(**bob_data)

    session.add(alice)
    session.add(bob)
    session.commit()

    alice_creds = base64.b64encode(b'{0}:{1}'.format(
        alice.username, alice_data['password'])).decode('utf-8')
    bob_creds = base64.b64encode(b'{0}:{1}'.format(
        bob.username, bob_data['password'])).decode('utf-8')

    response = client.get('/users/{}'.format(alice.id),
        headers={'Authorization': 'Basic ' + bob_creds})

    assert response.status_code == 403

    response = client.get('/users/{}'.format(bob.id),
        headers={'Authorization': 'Basic ' + alice_creds})

    assert response.status_code == 403
