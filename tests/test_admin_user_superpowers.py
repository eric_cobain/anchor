import base64
from flask import json, current_app
from app.models import User, Permission, Role, Delivery

def test_create_new_admin_user(db, session, client):
    """ Attempt to create an admin user.. this is by making sure
        the email matches the one in ANCHOR_ADMIN config
    """

    Role.insert_roles()
    data = {'username': 'Admin', 'email': 'yamamoto@admin.com',
            'password': 'bankai'}

    user = User(**data)
    session.add(user)
    session.commit()

    assert current_app.config['ANCHOR_ADMIN'] == 'yamamoto@admin.com'
    assert user.role == Role.query.filter_by(permissions=0xff).first()

def test_fetch_full_deliveries_list_as_admin(db, session, client):
    """
        Attempt to fetch full deliveries list from all users.
        An Admin user should be able to
    """
    data = {'username': 'Admin', 'email': 'yamamoto@admin.com',
            'password': 'bankai'}
    user = User(**data)
    session.add(user)
    session.commit()

    creds = base64.b64encode(
        b'{0}:{1}'.format(
            user.username, data['password'])).decode('utf-8')

    response = client.get('/deliveries',
                headers={'Authorization': 'Basic ' + creds})

    assert response.status_code == 200

def test_fetch_single_delivery_as_admin(db, session, client):
    """
        Attempt to fetch a delivery owned by another user as admin.
    """
    Role.insert_roles() # first create roles

    admin_data = {'username': 'Admin', 'email': 'yamamoto@admin.com',
            'password': 'bankai'}

    ikkaku_data = {'username': 'ikkaku', 'email': 'mandarame@11.com',
            'password': 'shikai'}
    admin = User(**admin_data)
    ikkaku = User(**ikkaku_data)
    session.add(admin)
    session.add(ikkaku)
    session.commit()

    creds = base64.b64encode(
        b'{0}:{1}'.format(
            admin.username, admin_data['password'])).decode('utf-8')


    delivery_data = {'item_name': 'iphone6', 'item_description': '6 gen iphone',
            'pickup': 'yaya center', 'dropoff': 'my house', 'recipient': 'me',
            'recipient_phone': 'my phone number'}

    delivery_data['user_id'] = ikkaku.id
    delivery = Delivery(**delivery_data)
    session.add(delivery)
    session.commit()

    response = client.get('/deliveries/{}'.format(delivery.id),
                headers={'Authorization': 'Basic ' + creds })

    assert current_app.config['ANCHOR_ADMIN'] == 'yamamoto@admin.com'
    assert admin.role == Role.query.filter_by(permissions=0xff).first()
    assert response.status_code == 200
    assert json.loads(response.get_data())['user_id'] == ikkaku.id
