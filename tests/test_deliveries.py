import base64
from flask import json
from app.models import Delivery, User

def test_create_new_delivery_authenticated(db, session, client):
    """ Attempt to create a delivery while authenticated."""

    delivery_data = {'item_name': 'iphone6', 'item_description': '6 gen iphone',
            'pickup': 'yaya center', 'dropoff': 'my house', 'recipient': 'me',
            'recipient_phone': 'my phone number'}

    user_data = {'username': 'authed', 'email':'authed@example',
                 'password': 'foobar'}
    user = User(**user_data)
    session.add(user)
    session.commit()

    creds = base64.b64encode(
        b'{0}:{1}'.format(
            user.username, user_data['password'])).decode('utf-8')

    response = client.post('/deliveries', data=delivery_data,
                    headers={'Authorization': 'Basic '+ creds})

    assert response.status_code == 201
    assert 'Location' in response.headers

    delivery = Delivery.query.filter(Delivery.item_name ==\
         delivery_data['item_name']).one()

    assert '/deliveries/{}'.format(delivery.id) in response.headers['Location']

def test_create_new_delivery_unauthenticated(db, session, client):
    """Attempt to create a delivery while unauthenticated."""

    data = {'item_name': 'iphone6', 'item_description': '6 gen iphone',
            'pickup': 'yaya center', 'dropoff': 'my house', 'recipient': 'me',
            'recipient_phone': 'my phone number'}


    response = client.post('/deliveries', data=data)
    assert response.status_code == 401

def test_create_invalid_delivery_authenticated(db, session, client):
    """
        Attempt to create delivery with invalid/missing information
        while authenticated.
    """

    delivery_data = {'item_name': 'iphone6', 'item_description': '6 gen iphone',
            'pickup': 'yaya center', 'dropoff': 'my house'}

    user_data = {'username': 'authed', 'email':'authed@example',
                 'password': 'foobar'}
    user = User(**user_data)
    session.add(user)
    session.commit()

    creds = base64.b64encode(
        b'{0}:{1}'.format(
            user.username, user_data['password'])).decode('utf-8')

    response = client.post('/deliveries', data=delivery_data,
                        headers={'Authorization': 'Basic ' + creds})
    assert response.status_code == 400
    assert 'message' in response.json
    assert 'recipient' in response.json['message']

def test_create_invalid_delivery_unauthenticated(db, session, client):
    """ Attempt to create delivery with invalid/missing information."""

    data = {'item_name': 'iphone6', 'item_description': '6 gen iphone',
            'pickup': 'yaya center', 'dropoff': 'my house'}

    response = client.post('/deliveries', data=data)
    assert response.status_code == 401

def test_get_single_delivery_authenticated(db, session, client):
    """Attempt fetching a single delivery record while authenticated."""

    user_data = {'username': 'authed', 'email':'authed@example',
                 'password': 'foobar'}
    user = User(**user_data)
    session.add(user)
    session.commit()

    creds = base64.b64encode(
        b'{0}:{1}'.format(
            user.username, user_data['password'])).decode('utf-8')

    delivery_data = {'item_name': 'iphone6', 'item_description': '6 gen iphone',
            'pickup': 'yaya center', 'dropoff': 'my house', 'recipient': 'me',
            'recipient_phone': 'my phone number'}

    delivery_data['user_id'] = user.id # make the user own the delivery.
    delivery = Delivery(**delivery_data)
    session.add(delivery)
    session.commit()

    response = client.get('/deliveries/{}'.format(delivery.id),
                            headers={'Authorization': 'Basic ' + creds})

    assert response.status_code == 200
    assert json.loads(response.get_data())['id'] == delivery.id

def test_get_single_delivery_unauthenticated(db, session, client):
    """Attempt fetching a single delivery record while authenticated."""

    delivery_data = {'item_name': 'iphone6', 'item_description': '6 gen iphone',
            'pickup': 'yaya center', 'dropoff': 'my house', 'recipient': 'me',
            'recipient_phone': 'my phone number'}

    delivery = Delivery(**delivery_data)
    session.add(delivery)
    session.commit()

    response = client.get('/deliveries/{}'.format(delivery.id))

    assert response.status_code == 401

def test_get_single_delivery_unauthorized(db, session, client):
    """Attempt fetching a single delivery record while authenticated."""

    vicky_data = {'username': 'vicky', 'email':'work@example',
                 'password': 'foobar'}

    urahara_data = {'username': 'kisuke', 'email':'urahara@13.com',
                 'password': 'bankai'}

    vicky = User(**vicky_data)
    urahara = User(**urahara_data)
    session.add(vicky)
    session.add(urahara)
    session.commit()

    creds = base64.b64encode(
        b'{0}:{1}'.format(
            urahara.username, urahara_data['password'])).decode('utf-8')

    delivery_data = {'item_name': 'iphone6', 'item_description': '6 gen iphone',
            'pickup': 'yaya center', 'dropoff': 'my house', 'recipient': 'me',
            'recipient_phone': 'my phone number'}

    delivery_data['user_id'] = vicky.id # make vicky own the delivery.
    delivery = Delivery(**delivery_data)
    session.add(delivery)
    session.commit()


    response = client.get('/deliveries/{}'.format(delivery.id),
                            headers={'Authorization': 'Basic ' + creds})

    assert response.status_code == 403

def test_fetch_delivery_list_authenticated(db, session, client):
    """Attempt fetching the deliveries list belonging to a user."""

    user_data = {'username': 'authed', 'email':'authed@example',
                 'password': 'foobar'}
    user = User(**user_data)
    session.add(user)
    session.commit()

    creds = base64.b64encode(
        b'{0}:{1}'.format(
            user.username, user_data['password'])).decode('utf-8')

    delivery_data = {'item_name': 'iphone6', 'item_description': '6 gen iphone',
            'pickup': 'yaya center', 'dropoff': 'my house', 'recipient': 'me',
            'recipient_phone': 'my phone number'}

    delivery_data['user_id'] = user.id # make the user own the delivery.
    delivery = Delivery(**delivery_data)
    session.add(delivery)
    session.commit()

    response = client.get('/deliveries',
                    headers={'Authorization': 'Basic ' + creds})

    assert response.status_code == 200
