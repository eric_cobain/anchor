import datetime

from sqlalchemy.ext.hybrid import hybrid_property
from flask import current_app

from app import db, flask_bcrypt

class Permission:
    HANDLE_DELIVERY = 0x01
    ADD_DELIVERY = 0x02
    MODERATE_DELIVERY = 0x04
    ADMINISTER = 0x80

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    @staticmethod
    def insert_roles():
        roles = {
            'User': (Permission.ADD_DELIVERY, True),
            'Moderator': (Permission.ADD_DELIVERY |
                          Permission.MODERATE_DELIVERY, False),
            'Administrator': (0xff, False)
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.permissions = roles[r][0]
            role.default = roles[r][1]
            print(role.name)
            db.session.add(role)
        db.session.commit()

    def __repr__(self):
        return '<Role %r>' % self.name

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    username = db.Column(db.String(40), unique=True, nullable=False)
    _password = db.Column('password', db.String(60), nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['ANCHOR_ADMIN']:
                self.role = Role.query.filter_by(permissions=0xff).first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()

    def __rep__(self):
        return '<User %r>' % self.username

    @hybrid_property
    def password(self):
        """The bcrypt'ed password of the user."""
        return self._password

    @password.setter
    def password(self, password):
        """ BCrypt the password on assignment."""
        self._password = flask_bcrypt.generate_password_hash(password)

    def can(self, permissions):
        return self.role is not None and\
            (self.role.permissions & permissions) == permissions

    def is_administrator(self):
        return self.can(Permission.ADMINISTER)

    @staticmethod
    def generate_fake_data(count=100):
        from sqlalchemy.exc import IntegrityError
        from random import seed
        import forgery_py

        seed()
        for i in range(count):
            user = User(username=forgery_py.internet.user_name(True),
                     email=forgery_py.internet.email_address(),
                     password=forgery_py.lorem_ipsum.word())
            db.session.add(user)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

class Delivery(db.Model):
    __tablename_ = 'deliveries'
    id = db.Column(db.Integer, primary_key=True)
    item_name = db.Column(db.String(255), nullable=False)
    item_description = db.Column(db.Text())
    pickup = db.Column(db.Text())
    dropoff = db.Column(db.Text())
    recipient = db.Column(db.String(255), nullable=False)
    recipient_phone  = db.Column(db.String(255), nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow,
                        index=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    user = db.relationship('User', backref=db.backref('deliveries',
                                                      lazy='dynamic'))

    def __repr__(self):
        return '<Delivery %r>' % self.item_name

    @staticmethod
    def generate_fake_data(count=100):
        from sqlalchemy.exc import IntegrityError
        from random import seed, randint
        import forgery_py

        seed()
        user_count = User.query.count()
        for i in range(count):
            user = User.query.offset(randint(0, user_count -1)).first()
            delivery = Delivery(
                item_name=forgery_py.name.company_name(),
                item_description=forgery_py.lorem_ipsum.sentences(randint(1, 4)),
                pickup=forgery_py.address.street_address(), # wish these were Kenyan
                dropoff=forgery_py.address.street_address(),
                recipient=forgery_py.name.first_name(),
                recipient_phone=randint(1000000000, 9999999999),
                user_id=user.id)
            db.session.add(delivery)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
