from flask import Flask, json, make_response
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.bcrypt import Bcrypt
from flask.ext.restful import Api
from flask.ext.httpauth import HTTPBasicAuth
from config import config

db = SQLAlchemy()
flask_bcrypt = Bcrypt()
api = Api()
auth = HTTPBasicAuth()

def create_app(config_name):
    app =Flask(__name__)

    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)
    flask_bcrypt.init_app(app)
    
    import authentication

    from .resources.deliveries import SingleDelivery, ListDelivery
    from .resources.users import SingleUser, CreateUser

    api.add_resource(ListDelivery, '/deliveries')
    api.add_resource(SingleDelivery, '/deliveries/<int:delivery_id>')
    api.add_resource(SingleUser, '/users/<int:user_id>')
    api.add_resource(CreateUser, '/users')

    @api.representation('application/json')
    def output_json(data, code, headers=None):
        response = make_response(json.dumps(data), code)
        response.headers.extend(headers or {})
        return response

    api.init_app(app)

    return app
