"""
    Delivery CRUD
"""
import sqlalchemy
from flask.ext.restful import Resource, abort, url_for, reqparse,\
    fields, marshal
from flask import g
from app import auth, db
from app.models import Delivery, Permission



class SingleDelivery(Resource):
    method_decorators = [auth.login_required]

    def get(self, delivery_id):
        """ GET requests """
        try:
            delivery = Delivery.query.filter(Delivery.id == delivery_id).one()
            if g.current_user.id != delivery.user_id and\
                    not g.current_user.can(Permission.MODERATE_DELIVERY):
                abort(403, message="You are not Authorized to access"
                        "this resource.")
        except sqlalchemy.orm.exc.NoResultFound:
            abort(404, message="No record of that delivery")

        data = dict(
            id=delivery_id,
            item_name = delivery.item_name,
            item_description = delivery.item_description,
            pickup = delivery.pickup,
            dropoff = delivery.dropoff,
            recipient = delivery.recipient,
            recipient_phone = delivery.recipient_phone,
            user_id = delivery.user_id,
            created = delivery.created,
        )

        return data, 200

    def delete(self, delivery_id):
        """ DELETE requests """
        pass

new_delivery_parser = reqparse.RequestParser()
new_delivery_parser.add_argument('item_name', type=str, required=True)
new_delivery_parser.add_argument('item_description', type=str, required=True)
new_delivery_parser.add_argument('pickup', type=str, required=True)
new_delivery_parser.add_argument('dropoff', type=str, required=True)
new_delivery_parser.add_argument('recipient', type=str, required=True)
new_delivery_parser.add_argument('recipient_phone', type=str, required=True)

deliveries_parser = reqparse.RequestParser()
deliveries_parser.add_argument('page', type=int, required=False,
    default=1, location='args')
deliveries_parser.add_argument('limit', type=int, required=False,
    default=20, location='args')

delivery_fields = {
    'item_name': fields.String,
    'item_description': fields.String,
    'pickup': fields.String,
    'dropoff': fields.String,
    'recipient': fields.String,
    'recipient_phone': fields.String,
    'created': fields.DateTime(dt_format='rfc822'),
}

class ListDelivery(Resource):
    method_decorators = [auth.login_required]

    def get(self):
        """ GET requests """
        data = deliveries_parser.parse_args(strict=True)
        offset = (data['page'] -1) * data['limit']
        if g.current_user.can(Permission.MODERATE_DELIVERY):
            deliveries = Delivery.query.order_by(
                Delivery.created.desc()).limit(
                data['limit']).offset(offset)
            return marshal(list(deliveries), delivery_fields), 200
 
        deliveries = g.current_user.deliveries.order_by(
            Delivery.created.desc()).limit(
            data['limit']).offset(offset)

        return marshal(list(deliveries), delivery_fields), 200

    def post(self):
        """ POST requests """
        data = new_delivery_parser.parse_args(strict=True)
        data['user_id'] = g.current_user.id
        delivery = Delivery(**data)
        db.session.add(delivery)

        try:
            db.session.commit()
        except sqlalchemy.exc.IntegrityError:
            #TODO figure out proper error message.
            abort(409, message="Error")

        data = dict(id=delivery.id,
                    item_name = delivery.item_name,
                    item_description = delivery.item_description,
                    pickup = delivery.pickup,
                    dropoff = delivery.dropoff,
                    recipient = delivery.recipient,
                    recipient_phone = delivery.recipient_phone,
                    user_id = delivery.user_id,
                    created = delivery.created)

        return data, 201, {'Location': url_for(
            'singledelivery', delivery_id=delivery.id, _external=True)}
