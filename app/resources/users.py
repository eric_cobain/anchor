"""
    Users CRUD
"""
import sqlalchemy
from flask.ext.restful import Resource, abort, url_for, reqparse
from flask import g
from app import auth, db
from app.models import User, Permission

new_user_parser = reqparse.RequestParser()
new_user_parser.add_argument('username', type=str, required=True)
new_user_parser.add_argument('email', type=str, required=True)
new_user_parser.add_argument('password', type=str, required=True)

class SingleUser(Resource):
    method_decorators = [auth.login_required]

    def get(self, user_id):
        """GET request"""
        if g.current_user.id != user_id and\
                  not g.current_user.can(Permission.ADMINISTER):
            abort(403, message="You have insufficient permissions"
                "to access thid resource.")
        try:
            user = User.query.filter(User.id == user_id).one()
        except sqlalchemy.orm.exc.NoResultFound:
            abort(404, message="No such user exists!")

        data = dict(
            id=user_id,
            username=user.username,
            email=user.email,
            created=user.created)

        return data, 200

class CreateUser(Resource):

    def post(self):
        """POST request"""
        data = new_user_parser.parse_args(strict=True)
        user = User(**data)
        db.session.add(user)

        try:
            db.session.commit()
        except sqlalchemy.exc.IntegrityError:
            db.session.rollback()
            abort(409, message="User already exists!")

        data = dict(id=user.id, username=user.username,
                    email=user.email, created=user.created)

        return data, 201, {'Location': url_for(
            'singleuser', user_id=user.id, _external=True)}

