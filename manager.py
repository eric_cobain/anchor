#!/usr/bin/env python

import os
from flask.ext.script import Manager, Shell
from flask.ext.migrate import Migrate, MigrateCommand
from app import create_app, db
from app.models import User, Delivery, Role

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)

def make_shell_context():
    return dict(app=app, db=db, User=User, Delivery=Delivery, Role=Role)

manager.add_command('shell', Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)

@manager.command
def setup_dev():
    from flask.ext.migrate import upgrade, init, migrate

    # initialize db
    init()

    # make migrations and place them in folder
    migrate()

    # migrate the db
    upgrade()

    # generate user roles
    Role.insert_roles()
    # fake users
    User.generate_fake_data()

    # fake deliveries
    Delivery.generate_fake_data()

if __name__ == '__main__':
    manager.run()
